package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;


public class IncrementTest {


@Test
    public void decreasecounterTest(int input) throws Exception{
    int results = new Increment().decreasecounter(0);
    assertEquals("Test Counter Class", 0, results);
    results = new Increment().decreasecounter(1);
    assertEquals("Test Counter Class", 1, results);
    results = new Increment().decreasecounter(2);
    assertEquals("Test Counter Class", 2, results);
}

} 
